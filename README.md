# Info
This code was written as an entry challenge when I entered Simbiose Ventures. Its goal was to 
imitate the behaviour that the analytics database from Simbiose Ventures had on the low level.
It was inspired by SAP HANA database and the specific detail I implemented is called a posting list.
At the time, I wrote all the comments in Portuguese. I think all variables are named in English though.
If you have any questions regarding this code, let me know.

It was written in Java.

### Objetivo
Este projeto tem por objetivo implementar um índice
Esse índice é implementado por meio de uma lista de nós, sendo cada nó uma tupla (chave, posting list).

    - Chave: um número identificador.
    - Posting list: uma lista de tuplas (id, contagem)

A classe para o posting list deve ser capaz de ser inicializada vazia ou a partir de um arquivo gerado pelo programa
anteriormente.

Os dados do programa são salvos em dois arquivos:
    - Arquivo de índices: (chave, posting list), contém a chave e o endereço do posting list no outro arquivo.
    - Posting lists: sequências de blocos de tamanhos fixos. Em cada bloco há um ponteiro indicando qual a próxima
    posição livre.

### Estrutura
#### Index
Início é de tamanho INDEX_HEADER_SIZE bytes
Primeiros 8 bytes do INDEX são o tamanho do arquivo em bytes
Próximos 4 bytes do INDEX é o número de pares (0 indica que só há o cabeçalho)
Após o cabeçalho, cada 8 bytes representam 4 bytes de um hash e 4 bytes de um endereço de postlist

#### Postlist
Cabeçalho no início é de tamanho PLIST_HEADER_SIZE bytes
BLOCK_SIZE bytes (contendo dados) seguidos por NEXT_ADDRESS_SIZE bytes (contendo endereço da continuação da fila)
