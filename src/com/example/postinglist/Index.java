package com.example.postinglist;

import java.util.Vector;

/**
 * Created by ataias on 4/11/16.
 */

//Reference: https://dzone.com/articles/whats-wrong-java-8-part-v
public class Index {
    public final Integer key;
    public final Integer postlist;

    public Index(Integer key, Integer postlist) {
        this.key = key;
        this.postlist = postlist;
    }
}