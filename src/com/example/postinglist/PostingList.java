package com.example.postinglist;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import java.util.Vector;

/**
 * Created by ataias on 4/11/16.
 */
public class PostingList {

    //CONSTANTES
    public final static int BLOCK_SIZE = 12; //multiplo de 4
    public final static int KEY_PLIST_PAIR_SIZE = 8;
    public final static int FIELD_SIZE = 4;
    public final static int NEXT_ADDRESS_SIZE = 4;
    public final static int INDEX_HEADER_SIZE = 100; //100 bytes
    public final static int PLIST_HEADER_SIZE = 100;
    public String indexesFilename;
    public String postingListsFilename;

    //Endereços no cabeçalho do índice
    public final static long INDEX_HEADER_FILE_LENGTH_ADDR = 0;
    public final static long INDEX_HEADER_N_ADDR = 8;

    //Endereços no cabeçalho do arquivo de posting lists
    public final static long PLIST_HEADER_FILE_LENGTH_ADDR = 0;
    public final static long PLIST_HEADER_N_ADDR = 8;

    byte[] emptyPost = new byte[BLOCK_SIZE+NEXT_ADDRESS_SIZE]; //initialize with 0 by default in java

    //indexes is a list of tuples (key, posting lists)
    //field is 32 bit unsigned integer with 2 parts:
    //Upper 20 bits: id
    //Lower 12 bits: contagem (a number that indicates the status)

    //an index is a tuple with (key, posting list)

//    Vector<Index> indexes;
    Vector<String> keys;
    byte[] bufferedPostingList = new byte[BLOCK_SIZE];

    //Create empty
    public PostingList(){
        indexesFilename = "indexes.bin";
        postingListsFilename = "postingList.bin";

        keys = new Vector<String>();
    }

    public PostingList(boolean continueFile) {
        indexesFilename = "indexes.bin";
        postingListsFilename = "postingList.bin";

        keys = new Vector<String>();

        if(!continueFile) {
            restartFiles();
        }
    }

    public void restartFiles() {
        clearFiles();
        setupHeaders();
    }

    private void setupHeaders() {
        try {
            //Header for Index file
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "rws");
            file.setLength(INDEX_HEADER_SIZE);

            //1) SIZE of file in Bytes
            file.seek(INDEX_HEADER_FILE_LENGTH_ADDR);
            file.writeLong(INDEX_HEADER_SIZE);

            //2) Número de pares
            file.seek(INDEX_HEADER_N_ADDR);
            file.writeInt(0);

            file.close();

            //Header for postlist file
            file = new RandomAccessFile(postingListsFilename, "rws");
            file.setLength(PLIST_HEADER_SIZE);

            //1) SIZE of file in Bytes
            file.seek(PLIST_HEADER_FILE_LENGTH_ADDR);
            file.writeLong(PLIST_HEADER_SIZE);

            //2) Número de blocos
            file.seek(PLIST_HEADER_N_ADDR);
            file.writeInt(0);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Após um aumento de 1 par, este método deve ser chamado para atualizar o cabeçalho
    private void incrementIndexNumberHeader() {
        long old = 0;
        try {
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "rws");
            //1) SIZE of file in Bytes
            file.seek(INDEX_HEADER_FILE_LENGTH_ADDR);
            old = file.readLong();
            file.seek(INDEX_HEADER_FILE_LENGTH_ADDR);
            file.writeLong(old + KEY_PLIST_PAIR_SIZE);

            //2) Número de pares
            file.seek(INDEX_HEADER_N_ADDR);
            old = file.readInt(); //4 bytes
            file.seek(INDEX_HEADER_N_ADDR);
            file.writeInt((int) (old + 1));

            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Adicionar um novo bloco e atualizar cabecalho
    private void incrementPlistNumberHeader() {
        long old = 0;
        try {
            RandomAccessFile file = new RandomAccessFile(postingListsFilename, "rws");
            //1) SIZE of file in Bytes
            file.seek(PLIST_HEADER_FILE_LENGTH_ADDR);
            old = file.readLong();
            file.seek(PLIST_HEADER_FILE_LENGTH_ADDR);
            file.writeLong(old + BLOCK_SIZE + NEXT_ADDRESS_SIZE);

            //2) Número de pares
            file.seek(PLIST_HEADER_N_ADDR);
            old = file.readInt(); //4 bytes
            file.seek(PLIST_HEADER_N_ADDR);
            file.writeInt((int) (old + 1));

            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearFiles() {
        try {
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "rws");
            file.setLength(0);
            file.close();

            file = new RandomAccessFile(postingListsFilename, "rws");
            file.setLength(0);
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Index readIndex(int position) {
        int key = 0, postinglist = 0;

        try {
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "r");
            file.seek(position);
            key = file.readInt();
            postinglist = file.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Index(key, postinglist);
    }

    //Adiciona indexes de forma que estão organizadas de forma crescente por hash value
    public Index createIndex(String key) {
        keys.add(key); //for further checking
        int hash = hashString(key);
        long newPostPos = 0; //endereço do postlist

        try {
            //Abrir arquivo
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "rw");

            //Total de pares (key, postinglist)
            file.seek(INDEX_HEADER_FILE_LENGTH_ADDR);
            long length = file.readLong(); //salvar tamanho do arquivo no arquivo

            //Descobrir ponto de inserção adequado para lista continuar ordenada
            long insertionPoint = INDEX_HEADER_SIZE; //ponto de insercao comeca depois do cabecalho
            for(long pos = INDEX_HEADER_SIZE; pos < length; pos += KEY_PLIST_PAIR_SIZE) {
                file.seek(pos);
                int hashRead = file.readInt();
                    if(hashRead < hash) {
                        insertionPoint = pos + KEY_PLIST_PAIR_SIZE;
                    } else {
                        break;
                    }
            }
            System.out.println("Insertion point for \"" + key +  "\" is " + Integer.toString((int)insertionPoint) +
            " as its value is " + Integer.toString(hash));

            for(long pos = length; pos > insertionPoint; pos -= KEY_PLIST_PAIR_SIZE) {
                file.seek(pos - KEY_PLIST_PAIR_SIZE);
                byte[] toMove = new byte[KEY_PLIST_PAIR_SIZE];
                file.read(toMove);

                file.seek(pos);
                file.write(toMove);
            }

            newPostPos = addPLIST();
            System.out.println("For " + key + ", address is " + Integer.toString((int) newPostPos));

            file.seek(insertionPoint);
            file.write(indexFromInts(hash, (int) newPostPos));
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        incrementIndexNumberHeader();
        return new Index(hash, (int) newPostPos);
    }

    private long addPLIST() {
        long newPostPos = 0;
        try {
            RandomAccessFile file = new RandomAccessFile(postingListsFilename, "rw");
            file.seek(PLIST_HEADER_FILE_LENGTH_ADDR);
            newPostPos = file.readLong();
            file.seek(newPostPos);
            file.write(emptyPost);
            file.close();
            incrementPlistNumberHeader();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return newPostPos;
    }

    private byte[] indexFromInts(int hash, int address) {
        return new byte[] {
                (byte) ((hash >>> 24) & 0xFF),
                (byte) ((hash >>> 16) & 0xFF),
                (byte) ((hash >>> 8) & 0xFF),
                (byte) (hash & 0xFF),
                (byte) ((address >>> 24) & 0xFF),
                (byte) ((address >>> 16) & 0xFF),
                (byte) ((address >>> 8) & 0xFF),
                (byte) (address & 0xFF)};
    }

    public void save() {

    }

    private long getIndexSize() {
        long size = 0;
        try {
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "r");
            file.seek(INDEX_HEADER_FILE_LENGTH_ADDR);
            size = file.readLong();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return size;
    }

    private int getPostlistAddress(int hash) {
        int address = 0;
        try {
            RandomAccessFile file = new RandomAccessFile(indexesFilename, "r");
            long index_size = getIndexSize();
            for(long pos = INDEX_HEADER_SIZE; pos < index_size; pos += KEY_PLIST_PAIR_SIZE) {
                file.seek(pos);
                int hashRead = file.readInt();
                int addressRead = file.readInt();
                if (hashRead == hash) address = addressRead;
            }
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    public void insertAtIndex(String key, int id, int count){
        int hash = hashString(key);
        insertAtIndex(hash, id, count);
    }

    public void insertAtIndex(Integer hash, int id, int count) {
        try {
            int postlistAddr = getPostlistAddress(hash);

            RandomAccessFile file = new RandomAccessFile(postingListsFilename, "rws");

            //Passo 1) Busca do ponto de inserção
            int insertionPoint = postlistAddr;
            outerloop:
            while (true) {
                for(int pos = postlistAddr; pos < postlistAddr + BLOCK_SIZE; pos += FIELD_SIZE) {
                    file.seek(pos);
                    int field = file.readInt();
                    if(getIdFromField(field) > id || field == 0) {
                        insertionPoint = pos;
                        break outerloop;
                    }
                }

                //if not found yet...
                file.seek(postlistAddr + BLOCK_SIZE);
                int nextAddress = file.readInt();
                if(nextAddress == 0) {
                    //bloco ficou cheio e próximo elemento está à direita de todos os id's presentes nesse bloco
                    insertionPoint = (int) addPLIST();

                    //Atualizar endereço do bloco anterior apontando para o próximo
                    file.seek(postlistAddr + BLOCK_SIZE);
                    file.writeInt(insertionPoint);
                } else {
                    postlistAddr = nextAddress; //continuar procurando no próximo
                }

            }

            //Passo 2) Inserção e reordenamento
            //variáveis auxiliares para reordenamento
            int left = 0, right = 0;

            int nextAddress = 0;
            left = getFieldFromIdAndCount(id, count);
            int pos = insertionPoint;
            do {
                file.seek(pos);
                right = file.readInt();
                file.seek(pos);
                file.writeInt(left);
                if (right == 0) {
                    break;
                } else {
                    left = right;
                }

                //próxima iteração
                if (pos < postlistAddr + BLOCK_SIZE) {
                    pos += FIELD_SIZE;
                } else {
                    file.seek(postlistAddr+BLOCK_SIZE);
                    nextAddress = file.readInt();

                    //caso especial no qual faltou espaço para o reordenamento
                    if(nextAddress == 0 && left != 0) {
                        nextAddress = (int) addPLIST();

                        //Atualizar endereço do bloco anterior apontando para o próximo
                        file.seek(postlistAddr + BLOCK_SIZE);
                        file.writeInt(nextAddress);
                        postlistAddr = nextAddress;
                        nextAddress = 0;
                        break;
                    }

                    postlistAddr = nextAddress;
                }
            } while (nextAddress > 0);

            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
    * id and count are numbers greater than 0
    * */
    private Integer getFieldFromIdAndCount(int id, int count) {
        Integer shiftedId = id << 12;
        Integer maskedCount = count & 0xFFF;
        Integer field = shiftedId | maskedCount;
        return field;
    }

    private int getIdFromField(int field){
        return field >>> 12;
    }

    private int getCountFromField(int field){
        return field & 0xFFF;
    }

    //from answer at: http://stackoverflow.com/questions/2624192/good-hash-function-for-strings
    private static int hashString(String str){
        int hash = 7;
        for (int i = 0; i < str.length(); i++) {
            hash = hash * 31  + str.charAt(i);
        }
        return hash;
    }

    public void printHashes() {
        System.out.println("String-hash:");
        for(int i = 0; i < keys.size(); i++){
            System.out.println(keys.get(i) + ": " + Integer.toString(hashString(keys.get(i))));
        }
        System.out.println();
    }

    //Query function to get list of ids according to the count value
    public int[] getIdsWithCount(Integer hash, int count) {
        int start = getPostlistAddress(hash);
        int nextAddress = 0;

        int[] v = new int[1000]; //máximo de valores é 1000
        int i = 0;

        out:
        try {
            RandomAccessFile file = new RandomAccessFile(postingListsFilename, "r");
            do {
                for (int pos = start; pos < start + BLOCK_SIZE; pos += FIELD_SIZE) {
                    file.seek(pos);
                    int field = file.readInt();
                    int count_ = getCountFromField(field);
                    int id_ = getIdFromField(field);
                    if (id_ == 0) break out;
                    if (count_ == count) {
                        v[i] = id_;
                        i++;
                    }
                }
                file.seek(start+BLOCK_SIZE);
                nextAddress = file.readInt();
                start = nextAddress;
            } while (nextAddress != 0);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return truncateArray(v, i);
    }

    public int getCountForId(int hash, int id) {
        int start = getPostlistAddress(hash);
        int nextAddress = 0;

        int count = -1; //-1 se não encontrado
        found:
        try {
            RandomAccessFile file = new RandomAccessFile(postingListsFilename, "r");
            do {
                for (int pos = start; pos < start + BLOCK_SIZE; pos += FIELD_SIZE) {
                    file.seek(pos);
                    int field = file.readInt();
                    int id_ = getIdFromField(field);
                    if (id_ == id) {
                        count = getCountFromField(field);
                        break found;
                    }
                }
                file.seek(start+BLOCK_SIZE);
                nextAddress = file.readInt();
                start = nextAddress;
            } while (nextAddress != 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int[] truncateArray(int[] a, int n) {
        int[] newArray = new int[n];
        for(int i = 0; i < n; i++) {
            newArray[i] = a[i];
        }

        return newArray;
    }
}
