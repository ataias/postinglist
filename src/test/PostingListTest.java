package test;

import com.example.postinglist.Index;
import com.example.postinglist.PostingList;

import java.io.IOException;
import java.io.RandomAccessFile;

import static org.junit.Assert.*;

/**
 * Created by ataias on 4/11/16.
 */
public class PostingListTest {

    PostingList postList;

    @org.junit.Before
    public void setUp() throws Exception {
        postList = new PostingList();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        postList = null;
    }

    @org.junit.Test
    public void createIndexTest() throws Exception {
        postList.restartFiles();
        int n = 0;
        postList.createIndex("gender"); n++;
        postList.createIndex("city"); n++;
        postList.createIndex("cida"); n++;
        postList.createIndex("cid2"); n++;
        postList.createIndex("c2da"); n++;
        postList.createIndex("oi!"); n++;

        //Verifica se tamanhos dos arquivos estão corretos
        try {
            RandomAccessFile file = new RandomAccessFile(postList.indexesFilename, "r");
            assertEquals(PostingList.INDEX_HEADER_SIZE + PostingList.KEY_PLIST_PAIR_SIZE*n, file.length());
            file.seek(PostingList.INDEX_HEADER_FILE_LENGTH_ADDR);
            long length = file.readLong();
            assertEquals(PostingList.INDEX_HEADER_SIZE + PostingList.KEY_PLIST_PAIR_SIZE*n, length);
            file.close();

            file = new RandomAccessFile(postList.postingListsFilename, "r");
            assertEquals(PostingList.PLIST_HEADER_SIZE + (PostingList.BLOCK_SIZE+PostingList.NEXT_ADDRESS_SIZE)*n,
                        file.length());
            file.seek(PostingList.PLIST_HEADER_FILE_LENGTH_ADDR);
            length = file.readLong();
            assertEquals(PostingList.PLIST_HEADER_SIZE + (PostingList.BLOCK_SIZE+PostingList.NEXT_ADDRESS_SIZE)*n,
                        length);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void insertValuesInPostListings() throws Exception {
        postList.restartFiles();

        Index gender = postList.createIndex("gender");
        //count 0: male
        //count 1: female
        int n = 20;
        for(int id = 1; id <= n; id++) {
            postList.insertAtIndex("gender", id, id % 2);
        }

        Index city = postList.createIndex("city");
        //número de cidades
        final int c = 3;

        for(int id = 1; id <= n; id++) {
            postList.insertAtIndex("city", id, (id - 1) % c);
        }

        //ID's de quem mora em CIDADE
        int CIDADE = 0; //número entre 0 e c-1
        int[] population = postList.getIdsWithCount(city.key, CIDADE);
        for(int i = 0; i < population.length; i++) {
            System.out.print(Integer.toString(population[i]) + " ");
        }
        System.out.println();

        //Para cada ID, verificar se é homem ou mulher
        int men_number = 0;
        int HOMEM = 0;
        for(int i = 0; i< population.length; i++) {
            int count = postList.getCountForId(gender.key, population[i]);
            if (count == HOMEM) {
                men_number++;
            }
        }

        System.out.println("Número de homens na cidade " + Integer.toString(CIDADE) + ": " + Integer.toString(men_number));

        //Expressão alternativa para número de homens na cidade:
        int n_homens = 0;
        int id = CIDADE + 1;
        while (id <= n) {
            if (id % 2 == 0) n_homens++;
            id += c;
        }
        assertEquals(n_homens, men_number);

    }
}